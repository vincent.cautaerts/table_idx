#[macro_use]
extern crate bencher;

use bencher::Bencher;
use std::collections::HashSet;
use table_idx::{set, Table};

struct MyData {
    uuid: u32,
    name: String,
}

fn make_table() -> Table<MyData> {
    let mut table = Table::<MyData>::new();
    for i in 0..100 {
        table.push(MyData {
            uuid: i,
            name: format!("Record({})", i),
        });
    }
    table
}

fn bench_it(bench: &mut Bencher, stored: bool) {
    let mut table = make_table();
    let searched = "Record(54)".to_string();
    let extract_name: fn(&MyData) -> HashSet<String> = |r| set![r.name.clone()];
    let index = table.create_index(extract_name, stored);
    bench.iter(|| {
        let _x = &table.get(&index, searched.clone(), 1)[0].record.name;
    })
}

fn bench_with_index(bench: &mut Bencher) {
    bench_it(bench, true)
}

fn bench_without_index(bench: &mut Bencher) {
    bench_it(bench, false)
}

fn bench_with_index_u32(bench: &mut Bencher) {
    let mut table = make_table();
    let searched = 54;
    let extract_uuid: fn(&MyData) -> HashSet<u32> = |r| set![r.uuid];
    let index = table.create_index(extract_uuid, true);
    bench.iter(|| {
        let _x = &table.get(&index, searched, 0)[0].record.name;
    })
}

benchmark_group!(
    benches,
    bench_with_index,
    bench_without_index,
    bench_with_index_u32
);
benchmark_main!(benches);
