//! The idea is what is expressed here:
//! <https://users.rust-lang.org/t/in-memory-databases-as-first-class-types/66604/13>
//! We just want to have a vector of some records, and be able
//! * to request to make an index (HashMap) on some data (made from a record)
//! * retrieve (efficiently) with this index
//! * off course, we should be able to have multiple indexes
//! * and multiple results for a query
//!
//! Care has been taken so that you can have multiple indexes to the same table,
//! as well as continue to modify the table (add/remove rows).

use std::any::Any;
use std::collections::{HashMap, HashSet};
use std::hash::Hash;
use std::marker::PhantomData;
use std::sync::atomic::{AtomicUsize, Ordering};

use thiserror::Error;

#[derive(Error, Debug)]
pub enum TableError {
    #[error("An index key, as calculated from the data, doesn't match the value in the index")]
    ConsistencyKeyMismatch,
    #[error("An index refers to an empty row")]
    ConsistencyEmptyRowInIndex,
    #[error("An index refers to a row_id above the current limit of row_ids")]
    ConsistencyRowIdOutOfBound,
    #[error("Downcasting the index failed")]
    DowncastingFailed,
    #[error("An index_id refers to a non-existing index")]
    ConsistencyMissingIndex,
}

/// Each row is identified by a 'RowId', which is not reused, even if the row is deleted.
pub type RowId = usize;
/// Each index has an ID
pub type IndexId = usize;

/// Trait to be implemented by objects able to extract some information from a record to be used
/// as an index. Each record can lead to multiple indexed information, hence the `Set` which is
/// returned.
pub trait KeyExtractor<Record, Key> {
    fn extract_key(&self, record: &Record) -> HashSet<Key>;
}
/// The simplest implementation of `KeyExtractor` is to have a function doing it, without
/// any state,...
struct SimpleFnKeyExtractor<Record, Key> {
    extract_key: fn(&Record) -> HashSet<Key>,
}
impl<Record, Key> KeyExtractor<Record, Key> for SimpleFnKeyExtractor<Record, Key> {
    fn extract_key(&self, record: &Record) -> HashSet<Key> {
        (self.extract_key)(record)
    }
}
impl<Record: 'static, Key: 'static> Into<Box<dyn KeyExtractor<Record, Key>>>
    for fn(&Record) -> HashSet<Key>
{
    fn into(self) -> Box<dyn KeyExtractor<Record, Key>> {
        Box::new(SimpleFnKeyExtractor { extract_key: self })
    }
}

/// Representation (for the user) of an index.
/// This light structure doesn't have a reference to the table.
/// But it is generic in the proper types to carry that information.
pub struct Index<Record, Key: Eq + Hash> {
    stored: bool,
    index_id: IndexId,
    phantom_data: PhantomData<(Record, Key)>,
}

/// Used to return matching rows from the table.
pub struct Row<'r, Record> {
    pub row_id: RowId,
    pub record: &'r Record,
}

// Ideally, we would get rid of the 'static lifetime on I
// but it's difficult, because Any<X> has a static requirement on X
struct IndexableMap<Record, Key: Eq + Hash + 'static> {
    index_map: HashMap<Key, HashSet<RowId>>,
    key_extractor: Box<dyn KeyExtractor<Record, Key>>,
}
impl<Record, Key: Eq + Hash + 'static> IndexableMap<Record, Key> {
    fn add_record(&mut self, record: &Record, row_id: RowId) {
        let keys = self.key_extractor.extract_key(record);
        for key in keys {
            self.index_map
                .entry(key)
                .or_insert_with(HashSet::new)
                .insert(row_id);
        }
    }
    fn remove_record(&mut self, record: &Record, row_id: RowId) {
        let keys = self.key_extractor.extract_key(record);
        for key in keys {
            if let Some(row_ids) = self.index_map.get_mut(&key) {
                row_ids.remove(&row_id);
            }
        }
    }
}

/// This counter helps detecting if an index constructed for one table is used
/// with another one.
static LAST_INDEX_ID: AtomicUsize = AtomicUsize::new(1);

type IndexData<Record> = (
    Box<dyn Any>,                          // really a IndexableMap<T,*>
    fn(&mut Box<dyn Any>, &Record, RowId), // function used to add a record to the indexable map
    fn(&mut Box<dyn Any>, &Record, RowId), // function used to remove a record from the indexable map
);

#[derive(Default)]
pub struct Table<Record: 'static> {
    records: Vec<Option<Record>>,
    indexes: HashMap<IndexId, IndexData<Record>>,
}
impl<Record> Table<Record> {
    pub fn iter(&self) -> impl Iterator<Item = &Record> {
        self.records.iter().flatten()
    }
}

impl<Record> Table<Record> {
    pub fn new() -> Table<Record> {
        Table {
            records: vec![],
            indexes: HashMap::new(),
        }
    }

    pub fn create_index<Key: Eq + Hash + 'static, F>(
        &mut self,
        into_key_extractor: F,
        stored: bool,
    ) -> Index<Record, Key>
    where
        F: Into<Box<dyn KeyExtractor<Record, Key>>>,
    {
        let index_id = LAST_INDEX_ID.fetch_add(1, Ordering::SeqCst) as IndexId;
        let key_extractor = into_key_extractor.into();
        // initial creation of the index
        let mut im = IndexableMap {
            index_map: HashMap::with_capacity(if stored { self.records.len() } else { 0 }),
            key_extractor,
        };
        if stored {
            for (row_id, r) in self.records.iter().enumerate() {
                if let Some(record) = r {
                    im.add_record(record, row_id as RowId);
                }
            }
            // setup to be able to update it later
            self.indexes.insert(
                index_id,
                (
                    Box::new(im),
                    |any, record, row_id| match any.downcast_mut::<IndexableMap<Record, Key>>() {
                        Some(im) => im.add_record(record, row_id),
                        _ => panic!("algo error 2"),
                    },
                    |any, record, row_id| match any.downcast_mut::<IndexableMap<Record, Key>>() {
                        Some(im) => im.remove_record(record, row_id),
                        _ => panic!("algo error 3"),
                    },
                ),
            );
        } else {
            self.indexes.insert(
                index_id,
                (
                    Box::new(im),
                    |_any, _record, _row_id| {},
                    |_any, _record, _row_id| {},
                ),
            );
        }
        // this serves as a handle for the user to specify the index
        Index {
            stored,
            index_id,
            phantom_data: PhantomData,
        }
    }

    pub fn drop_index<Key: Eq + Hash + 'static>(&mut self, index: Index<Record, Key>) {
        self.indexes.remove(&index.index_id);
    }

    pub fn push(&mut self, record: Record) -> RowId {
        let row_id = self.records.len() as RowId;
        self.records.push(Some(record));
        let record = self.records.last().unwrap().as_ref().unwrap();
        for val in self.indexes.values_mut() {
            (val.1)(&mut val.0, record, row_id);
        }
        row_id
    }

    pub fn remove(&mut self, row_id: RowId) -> Option<Record> {
        let t = self.records[row_id as usize].take();
        if let Some(t) = &t {
            for val in self.indexes.values_mut() {
                (val.2)(&mut val.0, t, row_id);
            }
        }
        t
    }

    pub fn get<Key: 'static + Eq + Hash>(
        &self,
        index: &Index<Record, Key>,
        key: Key,
        mut max: usize,
    ) -> Vec<Row<Record>> {
        if max == 0 {
            max = usize::MAX;
        }
        if let Some(h) = self.indexes.get(&index.index_id) {
            match h.0.downcast_ref::<IndexableMap<Record, Key>>() {
                Some(im) => {
                    if index.stored {
                        match im.index_map.get(&key) {
                            None => vec![],
                            Some(v) => v
                                .iter()
                                .filter_map(|row_id| {
                                    self.records[*row_id as usize].as_ref().map(|record| Row {
                                        row_id: *row_id,
                                        record,
                                    })
                                })
                                .take(max)
                                .collect(),
                        }
                    } else {
                        let mut ret = vec![];
                        for r in self.records.iter().enumerate() {
                            if let Some(record) = r.1 {
                                let keys = im.key_extractor.extract_key(record);
                                if keys.contains(&key) {
                                    ret.push(Row {
                                        row_id: r.0 as RowId,
                                        record,
                                    });
                                }
                            }
                            if ret.len() >= max {
                                break;
                            }
                        }
                        ret
                    }
                }
                None => panic!("wrong type for index?!"),
            }
        } else {
            panic!("Index not found!")
        }
    }

    pub fn check_consistency<Key: 'static + Eq + Hash>(
        &self,
        index: &Index<Record, Key>,
        full: bool,
    ) -> Result<(), TableError> {
        if let Some(h) = self.indexes.get(&index.index_id) {
            match h.0.downcast_ref::<IndexableMap<Record, Key>>() {
                Some(im) => {
                    if index.stored {
                        for (i, v) in &im.index_map {
                            for row_id in v {
                                if let Some(r) = self.records.get(*row_id as usize) {
                                    if let Some(r) = r {
                                        if full {
                                            let recalc_keys = im.key_extractor.extract_key(r);
                                            if !recalc_keys.contains(i) {
                                                return Err(TableError::ConsistencyKeyMismatch);
                                            }
                                            // note : we check that any 'i' (key in the index) is
                                            //  linked to a vector, but we don't check that all
                                            //  newly calculated keys are effectively somewhere.
                                        }
                                    } else {
                                        return Err(TableError::ConsistencyEmptyRowInIndex);
                                    }
                                } else {
                                    return Err(TableError::ConsistencyRowIdOutOfBound);
                                }
                            }
                        }
                        Ok(())
                    } else {
                        // no data to be checked. Verify that we can calculate each 'I' value
                        // without panicking...
                        if full {
                            for record in self.records.iter().flatten() {
                                let _key: HashSet<Key> = im.key_extractor.extract_key(record);
                            }
                        }
                        Ok(())
                    }
                }
                None => Err(TableError::DowncastingFailed),
            }
        } else {
            Err(TableError::ConsistencyMissingIndex)
        }
    }
}

#[macro_export]
macro_rules! set {
        [ $($e:expr),* $(,)? ] => {{
            let mut _s = std::collections::HashSet::new();
            $( _s.insert($e); )+
            _s
        }}
    }

#[cfg(test)]
mod tests {
    use std::collections::HashSet;

    use super::*;

    struct UserData {
        user_id: u32,
        name: &'static str,
    }

    #[test]
    fn it_works() -> Result<(), TableError> {
        env_logger::init();

        let mut user_table = Table::<UserData>::new();

        user_table.push(UserData {
            user_id: 1,
            name: "Valentine",
        });
        user_table.push(UserData {
            user_id: 42,
            name: "Dr Who",
        });
        user_table.push(UserData {
            user_id: 44,
            name: "Davros",
        });
        user_table.push(UserData {
            user_id: 24,
            name: "Dr Who",
        });

        // let index_by_uuid = user_table.create_index(|r: UserData| set![r.user_id], true);
        let extract_user_id: fn(&UserData) -> HashSet<u32> = |r: &UserData| set![r.user_id];
        let index_by_uuid = user_table.create_index(extract_user_id, true);

        user_table.check_consistency(&index_by_uuid, true)?;

        assert_eq!(0, user_table.get(&index_by_uuid, 54, 0).len());
        assert_eq!(
            user_table.get(&index_by_uuid, 44, 0)[0].record.name,
            "Davros"
        );

        user_table.push(UserData {
            user_id: 54,
            name: "The Master",
        });
        assert_eq!(
            user_table.get(&index_by_uuid, 54, 0)[0].record.name,
            "The Master"
        );

        user_table.check_consistency(&index_by_uuid, true)?;

        let extract_name: fn(&UserData) -> HashSet<&'static str> = |r: &UserData| set![r.name];
        let index_by_name = user_table.create_index(extract_name, true);

        assert_eq!(0, user_table.get(&index_by_name, "Amy", 0).len());
        assert_eq!(
            HashSet::from([24, 42]),
            user_table
                .get(&index_by_name, "Dr Who", 0)
                .into_iter()
                .map(|r| r.record.user_id)
                .collect()
        );

        // test of the 'max' parameter
        assert_eq!(2, user_table.get(&index_by_name, "Dr Who", 0).len());
        assert_eq!(1, user_table.get(&index_by_name, "Dr Who", 1).len());
        assert_eq!(2, user_table.get(&index_by_name, "Dr Who", 2).len());
        assert_eq!(2, user_table.get(&index_by_name, "Dr Who", 3).len());

        // example of doing an index on something calculated
        let extract_complex: fn(&UserData) -> HashSet<u32> =
            |r: &UserData| set![r.user_id + r.name.len() as u32];
        let index_by_something = user_table.create_index(extract_complex, true);
        assert_eq!(
            "Valentine",
            user_table.get(&index_by_something, 10, 0)[0].record.name
        );

        // test of removal
        let found = user_table.get(&index_by_name, "Dr Who", 0);
        let found_len = found.len();
        let first_row_id = found[0].row_id;
        let removed = user_table.remove(first_row_id).unwrap();
        assert_eq!(removed.name, "Dr Who");
        let found2 = user_table.get(&index_by_name, "Dr Who", 0);
        assert_eq!(found_len, found2.len() + 1);

        user_table.check_consistency(&index_by_name, true)?;

        // a test where the indexing function makes more than one key
        // FIXME: should be able to define the function inline when calling 'create_index'...
        let extract_chars: fn(&UserData) -> HashSet<char> = |r: &UserData| r.name.chars().collect();
        let index_chars = user_table.create_index(extract_chars, true);
        let records_where_name_contains_h = user_table.get(&index_chars, 'h', 0);
        assert_eq!(
            set!["Dr Who", "The Master"],
            records_where_name_contains_h
                .iter()
                .map(|r| r.record.name)
                .collect()
        );
        /* Fun: for each char, displays the name containing that char.
        for ch in 'a'..'z' {
            println!(
                "{} : {}", ch,
                user_table
                    .get(&index_chars, ch, 0)
                    .iter()
                    .map(|r| r.record.name)
                    .fold(String::new(), |mut a, b| {
                        a.reserve(b.len() + 2);
                        if a.len()>0 {
                            a.push_str(", ");
                        }
                        a.push_str(b);
                        a
                    })
            );
        }
         */

        // we can still iterate over all elements of the table
        let mut total_length = 0;
        for record in user_table.iter() {
            total_length += record.name.len();
        }
        assert_eq!(total_length, 31);

        drop(user_table);

        Ok(())
    }
}
