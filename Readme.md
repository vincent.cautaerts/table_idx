# table_idx

A table of records, with indexes for faster access.

# Basic usage

See the tests in `lib.rs`

```rust
use table_idx::{Table, set};

struct UserData {
    user_id: u32,
    name: &'static str,
}

fn main() {
    let mut user_table = Table::<UserData>::new();

    // add all your data
    user_table.push(UserData {
        user_id: 1,
        name: "Valentine",
    });
    // ...

    // create an index
    let index_by_name = user_table.create_index(|r| set![r.name], true);

    // add some more data
    user_table.push(UserData {
        user_id: 2,
        name: "Romeo",
    });
    
    // use index to search
    for record in  user_table.get(&index_by_name, "Valentine", 0) {
        // ...
    }
}

```